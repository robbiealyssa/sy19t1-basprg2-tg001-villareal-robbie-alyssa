#include "pch.h"
#include <iostream>
#include <string>
#include "Node.h"
#include <time.h>
using namespace std;


// note: sometimes I encounter exception thrown
//insert the first node
void insertFirstNode(Node *&head, Node *&tail, string name)
{
	Node *temp_Node = new Node;
	temp_Node->name = name;
	temp_Node->next = NULL;
	temp_Node->previous = NULL;
	head = temp_Node;
	tail = temp_Node;

	//delete node
	delete temp_Node;
}

//insert next and make last node connect to first node
Node* insertNode(int &sizeCount)
{
	string name;
	Node* head = NULL;
	Node* tail = NULL;
	//int count = 0;

	//do
	//{
		for (int i = 0; i < sizeCount; i++)
		{
			cout << "Enter Name " << "[" << i + 1 << "]: ";
			cin >> name;
			Node* member = new Node;
			member->name = name;
			if (head == NULL)
			{
				head = member;
				tail = member;
			}
			else
			{
				tail->next = member;
				tail = tail->next;
			}
			//sizeCount++;
		}
	//} while (count != sizeCount);
	return head;
	//delete node

}

//show the namelist
void printListNames(Node *&head, int &sizeCount)
{
	Node *current_Node = head;
	while (current_Node != NULL)
	{
		cout << current_Node->name << endl;
		current_Node = current_Node->next;
		//current_Node = current_Node->previous;   (?)
	}

}

//after randomizer, delete
Node* deleteNode(Node* head, int &sizeCount)
{
	Node *temp = head;
	// remove node
	cout << "Removed: " << temp->name << endl;
	temp->previous->next = temp->next;
	temp->next->previous = temp->previous;
	///temp->previous = temp->next;
	temp->next = head;

	sizeCount--;
	//delete node;
	delete temp;
	return head;
	//temp->prev->next = temp->next;
	// temp->next->prev= temp->prev;
}
 
//make randomizer         
Node* generateRandomNode(Node *head, int &sizeCount)
{
	Node *randomNode = head;
	int randomNumber = rand() % sizeCount + 1;
	//int pickedNumber = rand() % sizeCount + 1;

	for (int i = 0; i < randomNumber; i++)
	{
		randomNode = randomNode->next;
	}
	cout << randomNode->name << " Has been chosen and have Drawn " << randomNumber << endl;
	head = randomNode;
	return head;
	delete randomNode;
}

//during rounds
void generateRounds(Node *head, int sizeCount)
{
		cout << "==============================" << endl;
		cout << "Remaining Memebers: " << endl;
		printListNames(head, sizeCount);
		cout << "==============================" << endl;
		// start randomize
		head = generateRandomNode(head, sizeCount);
		//delete node                                ////{EXCEPTION THROWN}
		//head = deleteNode(head, sizeCount);   /// error:exception thrown
}

// extra functions
void clearScreen()
{
	system("pause");
	system("cls");
}

int main()
{
	srand(time(NULL));

	Node* head;
	Node* tail;
	int sizeCount = 5;

	cout << "Enter 5 names..." << endl;
	clearScreen();
	head = insertNode(sizeCount);

	for (int i = 0; i < sizeCount; i++)
	{
		clearScreen();
		cout << "Round  :: " << " [" << i + 1 << "]" << endl;
		generateRounds(head, sizeCount);


	}
	
	return 0;
}







////Reference:
////https://www.youtube.com/watch?v=pBaZl9B448g
////https://www.geeksforgeeks.org/circular-linked-list-set-2-traversal/
////https://www.codementor.io/codementorteam/a-comprehensive-guide-to-implementation-of-singly-linked-list-using-c_plus_plus-ondlm5azr
////https://www.geeksforgeeks.org/select-a-random-node-from-a-singly-linked-list/
////https://www.youtube.com/watch?v=iWS6qmtppcU
////https://www.youtube.com/watch?v=U-MfAoL6qjM
////https://www.youtube.com/watch?v=Y0n86K43GO4
