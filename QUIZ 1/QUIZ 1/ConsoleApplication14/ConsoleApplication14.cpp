#include "pch.h"
#include <iostream>
#include <string>
using namespace std;

void printPriceList(string pckgeArray[], int priceArray[], int sizePckge, int sizePrice)
{
	int temp;
	for (int first = 0; first < sizePrice; first++)
	{
		for (int next = first; next < sizePrice; next++)
		{
			if (priceArray[first] > priceArray[next])
			{
				temp = priceArray[first];
				priceArray[first] = priceArray[next];
				priceArray[next] = temp;
			}
		}
	}

	for (int i = 0; i < sizePckge; i++)
	{
		for (int j = 0; j < sizePrice; j++)
		{
			cout << pckgeArray[j] << " =-> " << priceArray[j] << endl;
		}
		break;
	}
}
//------------------------------------------------------------------------------------------------------------------------------------------
void Q_User()
{
	cout << "How much would you like to buy? ";
}

void Q_UserAgain()
{
	cout << "Thankyou for purchasing. Would you like to buy again? {[y]-yes || [n]-no} => ";
}

void suggest_Pckge(string pckgeArray)
{
	cout << "Insuffeicient Gold. Would you like to buy " << pckgeArray << " ? {[y]-yes || [n]-no} => ";
}

void exit()
{
	cout << "ThankYou for purchsing." << endl;
}
//---------------------------------------------------------------------------------------------------------------------------------------
int lessInitialGold(int initialGold, int &totalGold, int userInputGold)
{
	totalGold = initialGold - userInputGold;
	cout << "You now have: " << totalGold << endl;
	return totalGold;
}

int greaterInitialGold(int initialGold, int &totalGold, int userInputGold)
{
	totalGold = userInputGold - initialGold;
	cout << "Insufficient Gold..." << endl;
	return totalGold;
}

int passTotalGold(int &initialGold, int &totalGold, int currentTotalGold)
{
	currentTotalGold = initialGold;
	initialGold = totalGold;
	totalGold = currentTotalGold;
	return currentTotalGold;

}

int acceptpackage(int initialGold, int &totalGold, int userInputGold, int suggestedPrice)
{
	totalGold = (totalGold + suggestedPrice)- userInputGold;
	cout << "=======================================" << endl;
	cout << "You now have: " << totalGold << endl;
	cout << "=======================================" << endl;
	return totalGold;
}

int subtractAfterPckge(int &totalGold, int userInputGold)
{
	totalGold = totalGold - userInputGold;
	cout << "=======================================" << endl;
	cout << "You now have: " << totalGold << endl;
	cout << "=======================================" << endl;
	return totalGold;
}
//---------------------------------------------------------------------------------------------------------------------------------------
void decisionToBuy_Again(char userDecision)
{
	switch (userDecision)
	{
	case'y':
		break;
	case'n':
		exit();
		exit(1);
		break;
	}
}

void afterPckgeBuy_Again(char userDecision, int initialGold, int &totalGold, int currentTotalGold)
{
	switch (userDecision)
	{
	case'y':
		passTotalGold(initialGold, totalGold, currentTotalGold);
		break;
	case'n':
		exit();
		exit(1);
		break;
	}
}

void decisionToBuy_Pckge(char userDecision, int initialGold, int &totalGold, int userInputGold, int suggestedPrice)
{
	switch (userDecision)
	{
	case'y':
		acceptpackage(initialGold, totalGold, userInputGold, suggestedPrice);
		//subtractAfterPckge(totalGold, userInputGold);
		break;
	case'n':
		exit();
		exit(1);
		break;
	}
}
//---------------------------------------------------------------------------------------------------------------------------------------
int main()
{
	string pckgeArray[] = { "Package [1]", "Package [2]" , "Package [3]", "Package [4]", "Package [5]", "Package [6]", "Package [7]" };
	int priceArray[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	int sizePckge = sizeof(pckgeArray) / sizeof(string);       // or =sizeof(pckgeArray) / sizeof(pckgeArray[0])
	int sizePrice = sizeof(priceArray) / sizeof(int);

	int initial_Gold = 250;
	int total_Gold = 0;
	int currentTotal_Gold = 0;
	int userInput_Gold;
	char userDecision;

	cout << "List of Packages:" << endl;
	printPriceList(pckgeArray, priceArray, sizePckge, sizePrice);
	cout << "++++++++++++++++++++++++++++++++++++++" << endl;

	system("pause");
	cout << endl;

	while (true)
	{
		cout << "=======================================" << endl;
		cout << "Your Current Gold: " << initial_Gold << endl;
		cout << "=======================================" << endl;
		Q_User();
		cin >> userInput_Gold;
		cout << endl;

		if (userInput_Gold <= initial_Gold)
		{
			lessInitialGold(initial_Gold, total_Gold, userInput_Gold);
			Q_UserAgain();
			cin >> userDecision;
			decisionToBuy_Again(userDecision);
			passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
			cout << endl;
		}

		if (userInput_Gold > initial_Gold)
		{
			//greaterInitialGold(initial_Gold, total_Gold, userInput_Gold);
			
			if (userInput_Gold >= priceArray[0] && userInput_Gold < priceArray[1])
			{
				cout << endl;
				suggest_Pckge(pckgeArray[0]);
				passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
				cin >> userDecision;
				decisionToBuy_Pckge(userDecision, initial_Gold, total_Gold, userInput_Gold, priceArray[0]);
				passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
				Q_UserAgain();
				cin >> userDecision;
				afterPckgeBuy_Again(userDecision, initial_Gold, total_Gold, currentTotal_Gold);
				passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
				cout << endl;
			}
			else if (userInput_Gold >= priceArray[1] && userInput_Gold < priceArray[2])
			{
				cout << endl;
				suggest_Pckge(pckgeArray[1]);
				passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
				cin >> userDecision;
				decisionToBuy_Pckge(userDecision, initial_Gold, total_Gold, userInput_Gold, priceArray[1]);
				passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
				Q_UserAgain();
				cin >> userDecision;
				afterPckgeBuy_Again(userDecision, initial_Gold, total_Gold, currentTotal_Gold);
				passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
				cout << endl;
			}
			else if (userInput_Gold >= priceArray[2] && userInput_Gold < priceArray[3])
			{
				cout << endl;
				suggest_Pckge(pckgeArray[2]);
				cin >> userDecision;
				decisionToBuy_Pckge(userDecision, initial_Gold, total_Gold, userInput_Gold, priceArray[2]);
				Q_UserAgain();
				cin >> userDecision;
				afterPckgeBuy_Again(userDecision, initial_Gold, total_Gold, currentTotal_Gold);
				passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
				cout << endl;
			}
			else if (userInput_Gold >= priceArray[3] && userInput_Gold < priceArray[4])
			{
				cout << endl;
				suggest_Pckge(pckgeArray[3]);
				cin >> userDecision;
				decisionToBuy_Pckge(userDecision, initial_Gold, total_Gold, userInput_Gold, priceArray[3]);
				Q_UserAgain();
				cin >> userDecision;
				afterPckgeBuy_Again(userDecision, initial_Gold, total_Gold, currentTotal_Gold);
				passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
				cout << endl;
			}
			else if (userInput_Gold >= priceArray[4] && userInput_Gold < priceArray[5])
			{
				cout << endl;
				suggest_Pckge(pckgeArray[4]);
				cin >> userDecision;
				decisionToBuy_Pckge(userDecision, initial_Gold, total_Gold, userInput_Gold, priceArray[4]);
				Q_UserAgain();
				cin >> userDecision;
				afterPckgeBuy_Again(userDecision, initial_Gold, total_Gold, currentTotal_Gold);
				passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
				cout << endl;
			}
			else if (userInput_Gold >= priceArray[5] && userInput_Gold < priceArray[6])
			{
				cout << endl;
				suggest_Pckge(pckgeArray[5]);
				cin >> userDecision;
				decisionToBuy_Pckge(userDecision, initial_Gold, total_Gold, userInput_Gold, priceArray[5]);
				Q_UserAgain();
				cin >> userDecision;
				afterPckgeBuy_Again(userDecision, initial_Gold, total_Gold, currentTotal_Gold);
				passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
				cout << endl;
			}
			else if (userInput_Gold >= priceArray[6] && userInput_Gold < priceArray[1])
			{
				cout << endl;
				suggest_Pckge(pckgeArray[6]);
				cin >> userDecision;
				decisionToBuy_Pckge(userDecision, initial_Gold, total_Gold, userInput_Gold, priceArray[6]);
				Q_UserAgain();
				cin >> userDecision;
				afterPckgeBuy_Again(userDecision, initial_Gold, total_Gold, currentTotal_Gold);
				passTotalGold(initial_Gold, total_Gold, currentTotal_Gold);
				cout << endl;
			}

		}

	}
	return 0;
}

