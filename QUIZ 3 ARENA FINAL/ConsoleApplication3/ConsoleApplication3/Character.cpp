#include "pch.h"
#include "Character.h"

Character::Character(){
	mName = getPlayerName();
	mPlayerClass = getPlayerClass();
	mHp = getHp();
	mPow = getPow();
	mVit = getVit();
	mAgi = getAgi();
	mDex = getDex();
}

Character::Character(string name, string playerClass, int hp, int pow, int vit, int agi, int dex){
	mName = name;
	mPlayerClass = playerClass;
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mAgi = agi;
	mDex = dex;
}

void Character::displayStats(string name){
	cout << "Name: " << name << endl;
	cout << "Class: " << mPlayerClass << endl;
	cout << "HP: " << mHp << endl;
	cout << "Pow: " << mPow << endl;
	cout << "Vit: " << mVit << endl;
	cout << "Agi: " << mAgi << endl;
	cout << "Dex: " << mAgi << endl;
}

void Character::displayEnemyStats(string name)
{
	cout << "Name: " << name << endl;
	cout << "Class: " << mPlayerClass << endl;
	cout << "HP: " << mHp << endl;
	cout << "Pow: " << mPow << endl;
	cout << "Vit: " << mVit << endl;
	cout << "Agi: " << mAgi << endl;
	cout << "Dex: " << mAgi << endl;
}

string Character::getPlayerName(){
	return mName;
}

string Character::getPlayerClass(){
	return mPlayerClass;
}

int Character::getHp(){
	return mHp;
}

int Character::getPow(){
	return mPow;
}

int Character::getVit(){
	return mVit;
}

int Character::getAgi(){
	return mAgi;
}

int Character::getDex(){
	return mDex;
}

int Character::getHitRate()
{
	int hit = (mDex / mAgi) * 100;
	return hit;
}

void Character::getDamage(int rndDamage)
{
	mHp -= rndDamage;
	if (mHp < 0) {
		mHp = 0;
	}
}

void Character::getDamageWithBonus(int rndDamage)
{
	//mHp = (mPow - mVit) * 50??
}

void Character::warriorWin(){
	
	this->mHp += 3;
	this->mVit += 3;
}

void Character::assassinWin(){
	this->mAgi += 3;
	this->mDex += 3;
}

void Character::mageWin(){
	 this->mPow += 3;
}



//int Character::getDamage(int rndDamage)
//{
//	this->mHp -= rndDamage;
//	return mHp;
//}

