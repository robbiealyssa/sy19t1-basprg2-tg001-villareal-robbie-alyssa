#pragma once
#include "Item.h"
#include <iostream>
#include <string>

using namespace std;

class SR :
	public Item
{
public:
	SR();
	~SR();

	void obtainItem(Player* collector);

private: 
	int SRadd;
};

