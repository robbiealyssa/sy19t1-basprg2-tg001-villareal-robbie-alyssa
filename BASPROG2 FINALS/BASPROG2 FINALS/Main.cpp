#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include "Player.h"
#include "Item.h"
#include "GachaMachine.h"

using namespace std;

void cls() {
	system("pause");
	system("cls");
}

int main() {
	srand(time(NULL));
	Player* player = new Player(100, 100, 0, 0);
	GachaMachine* machine = new GachaMachine();

	while (player->getHp() > 0) {
		//conditions: when HP == 0 end program LOSE
		//          : when Crystal == 0 endd program LOSE
		//          : when Rarity Ponits == 100 end program WIN 
		// deduct crystal every turn

		player->displayPlayerStats();

		system("pause");

		player->payCrytal();
		cout << endl;
		
		machine->obtainItem(player);
		machine->itemChances(player);

		cls();

		if (player->getRarityPoints() >= 100) {
			cout << "You win" << endl;
			cls();
			system("pause");
			break;
			
		}

		if (player->getCrystal() == 0) {
			cout << "Yo lost" << endl;
			cls();
			system("pause");
			break;
		}

	}

	cout << "+++++++++++++++++++++++" << endl;
	player->displayPlayerStats();
	cout << "+++++++++++++++++++++++" << endl;

	cout << "Items Pulled:" << endl;
	player->displayObtainedItems();



	delete player;
	delete machine;

	system("pause");
	return 0;
}