#include "R.h"
#include "Player.h"
#include <iostream>
#include <string>

using namespace std;

R::R(){
	Radd = 1;
	//itemPullCounter = 0;
}

R::~R(){
}

void R::obtainItem(Player * collector){
	cout << "Obtained an R " << endl;
	cout << "You received 1 Rarity Point" << endl;
	collector->addR(this->Radd);
	collector->getRPulls();
}
