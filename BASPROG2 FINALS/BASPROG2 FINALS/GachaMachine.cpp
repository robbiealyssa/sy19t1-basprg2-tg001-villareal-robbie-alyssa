#include "GachaMachine.h"
#include "Player.h"
#include "Item.h"
#include "SSR.h"
#include "SR.h"
#include "R.h"
#include "healthPotion.h"
#include "Crystal.h"
#include "Bomb.h"
#include <time.h>

GachaMachine::GachaMachine(){
	addItem.push_back(new SSR); 
	addItem.push_back(new SR);
	addItem.push_back(new Crystal);
	addItem.push_back(new healthPotion);
	addItem.push_back(new Bomb);
	addItem.push_back(new R);
}

GachaMachine::~GachaMachine(){
}

void GachaMachine::itemChances(Player * player){
	//srand(time(NULL));
	
	int randItem = rand() % 100 + 1; 

	if (randItem == 1) {
		addItem[0]->obtainItem(player);
	}
	else if (randItem >= 2 && randItem <= 10) {          
		addItem[1]->obtainItem(player);
	}
	else if (randItem >= 11 && randItem <= 25) { 
		addItem[2]->obtainItem(player);
	}
	else if (randItem >= 26 && randItem <= 40) { 
		addItem[3]->obtainItem(player);
	}
	else if (randItem >= 41 && randItem <= 60) {             
		addItem[4]->obtainItem(player);
	}
	else if (randItem >= 61 && randItem <= 100) {            
		addItem[5]->obtainItem(player);
	}

}


