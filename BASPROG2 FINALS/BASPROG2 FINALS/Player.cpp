#include "Player.h"
#include "Item.h"
#include "GachaMachine.h"

Player::Player(int hp, int crystal, int rarityPoints, int pulls){
	mHp = hp;
	mCrystal = crystal;
	mRarityPoints = rarityPoints;
	mPulls = pulls;

	mCrystalPulls = 0;
	mHealthPotionPulls = 0;
	mBombPulls = 0;
	mSSRpulls = 0;
	mSRpulls = 0;
	mRpulls = 0;
}

Player::~Player(){
}

int Player::getHp(){
	if (mHp <= 0) mHp = 0;
	return mHp;
}

int Player::getCrystal(){
	return mCrystal;    
}

int Player::getRarityPoints(){
	return mRarityPoints;
}

int Player::getPulls(){
	return mPulls++;
}

//

int Player::getCrystalPulls() {
	return mCrystalPulls++;
}

int Player::getHealthPotionPulls(){
	return mHealthPotionPulls++;
}

int Player::getBombPulls(){
	return mBombPulls++;
}

int Player::getRPulls(){
	return mRpulls++;
}

int Player::getSRPulls(){
	return mSRpulls++;
}

int Player::getSSRPulls(){
	return mSSRpulls++;
}

//

int Player::addHp(int aHp){
	return mHp += aHp;
}

int Player::ReduceHp(int dealDmg){
	return mHp -= dealDmg;
}

int Player::ReduceCrystal(int payCrystal){
	return mCrystal -= payCrystal;
}

int Player::addSSR(int aRarityPoints){
	return mRarityPoints += aRarityPoints;
}

int Player::addSR(int aRarityPoints){
	return mRarityPoints += aRarityPoints;
}

int Player::addR(int aRarityPoints){
	return mRarityPoints += aRarityPoints;
}

int Player::addCrystal(int obtainCrystal){
	return mCrystal += obtainCrystal;
}


int Player::payCrytal(){
	return mCrystal -= 5;
}

void Player::displayPlayerStats(){
	cout << "HP: " << getHp() << endl;
	cout << "Crystals: " << getCrystal() << endl;
	cout << "Rarity Points: " << getRarityPoints() << endl;
	cout << "Pulls: " << getPulls() << endl;
}

void Player::displayObtainedItems(){
	cout << "SSR: " << getSSRPulls() << "X" << endl;
	cout << "SR: " << getSRPulls() << "X" << endl;
	cout << "R: " << getRPulls() << "X" << endl;
	cout << "Crystal: " << getCrystalPulls() << "X" << endl;
	cout << "Health Potion: " << getHealthPotionPulls() << "X" << endl;
	cout << "Bomb: " << getBombPulls() << "X" << endl;
}

