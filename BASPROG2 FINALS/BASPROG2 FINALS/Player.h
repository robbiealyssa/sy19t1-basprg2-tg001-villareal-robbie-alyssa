#pragma once
#include "Item.h"
#include <iostream>
#include <string>

using namespace std;

class Item;
class Player{
public:
	Player(int hp, int crystal, int rarityPoints, int pulls);
	~Player();

	int getHp();
	int getCrystal();
	int getRarityPoints();
	int getPulls();

	// pulls of items: increment
	int getCrystalPulls();
	int getHealthPotionPulls();
	int getBombPulls();
	int getRPulls();
	int getSRPulls();
	int getSSRPulls();

	// applies after obtaining the item
	int addHp(int aHp);
	int ReduceHp(int dealDmg);
	int ReduceCrystal(int payCrystal);
	int addSSR(int aRarityPoints);
	int addSR(int aRarityPoints);
	int addR(int aRarityPoints);
	int addCrystal(int obtainCrystal);

	int payCrytal();

	//display
	void displayPlayerStats();

	//for summary of items pulled
	void displayObtainedItems();

private:
	int mHp, mCrystal, mRarityPoints, mPulls;
	
	//ItemPulls;
	int mCrystalPulls, mHealthPotionPulls, mBombPulls, mSSRpulls, mSRpulls, mRpulls;
};

