#include "SSR.h"
#include "Player.h"
#include <iostream>
#include <string>

using namespace std;

SSR::SSR(){
	SSRadd = 50;
}

SSR::~SSR(){
}

void SSR::obtainItem(Player * collector){
	cout << "Obtained an SSR " << endl;
	cout << "You received 50 Rarity Points" << endl;
	collector->addSSR(this->SSRadd);
	collector->getSSRPulls();
}


