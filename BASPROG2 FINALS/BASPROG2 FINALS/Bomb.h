#pragma once
#include "Item.h"
class Bomb :
	public Item
{
public:
	Bomb();
	~Bomb();

	void obtainItem(Player* collector);

private:
	int bomb;
};

