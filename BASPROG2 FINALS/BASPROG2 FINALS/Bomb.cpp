#include "Bomb.h"
#include "Player.h"
#include <iostream>
#include <string>

using namespace std;

Bomb::Bomb(){
	bomb = 25;
}

Bomb::~Bomb(){
}

void Bomb::obtainItem(Player * collector){
	cout << "Obtained a bomb " << endl;
	cout << "Deal 25 Damage to HP " << endl;
	collector->ReduceHp(this->bomb);
	collector->getBombPulls();
}
