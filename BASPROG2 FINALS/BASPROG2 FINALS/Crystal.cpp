#include "Crystal.h"
#include "Player.h"
#include <iostream>
#include <string>

using namespace std;

Crystal::Crystal(){
	crystalAdd = 15;
	/*this->itemPullCounter = 1;*/
	//itemPullCounter = 0;
}

Crystal::~Crystal(){
}

void Crystal::obtainItem(Player * collector) {
	cout << "Obtained a Crystal " << endl;
	cout << "You have gained 15 Crystals " << endl;
	collector->addCrystal(this->crystalAdd);
	collector->getCrystalPulls();

}
