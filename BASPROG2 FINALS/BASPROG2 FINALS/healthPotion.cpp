#include "healthPotion.h"
#include "Player.h"
#include <iostream>
#include <string>

using namespace std;

healthPotion::healthPotion(){
	hpAdd = 30;
}

healthPotion::~healthPotion(){
}

void healthPotion::obtainItem(Player * collector){
	cout << "Obtained a Health Potion " << endl;
	cout << "You have gained 50 HP " << endl;
	collector->addHp(this->hpAdd);
	collector->getHealthPotionPulls();
}
