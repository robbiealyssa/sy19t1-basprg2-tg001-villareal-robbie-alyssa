#pragma once
#include "Item.h"
class Crystal :
	public Item
{
public:
	Crystal();
	~Crystal();

	void obtainItem(Player* collector);

private:
	int crystalAdd;
	
};

