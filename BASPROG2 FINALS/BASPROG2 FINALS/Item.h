#pragma once
#include <iostream>
#include <string>

using namespace std;

class Player;
class Item
{
public:
	Item();
	~Item();

	//polymorphism
	virtual void obtainItem(Player* collector);

protected:
	//int itemPullCounter;
};

