#pragma once
#include "Item.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Player;
class Item;
class GachaMachine :
	public Item
{
public:
	GachaMachine();
	~GachaMachine();

	// random chances
	void itemChances(Player* player);

private:
	vector<Item*>addItem;
};
