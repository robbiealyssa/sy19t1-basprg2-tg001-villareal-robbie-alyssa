#pragma once
#include "Item.h"
class healthPotion :
	public Item
{
public:
	healthPotion();
	~healthPotion();

	void obtainItem(Player* collector);

private:
	int hpAdd;
};

