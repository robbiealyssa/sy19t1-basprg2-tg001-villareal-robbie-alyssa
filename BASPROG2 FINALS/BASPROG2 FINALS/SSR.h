#pragma once
#include "Item.h"
#include <iostream>
#include <string>

using namespace std;

class SSR :
	public Item
{
public:
	SSR();
	~SSR();

	void obtainItem(Player* collector);

private:
	int SSRadd;
};

