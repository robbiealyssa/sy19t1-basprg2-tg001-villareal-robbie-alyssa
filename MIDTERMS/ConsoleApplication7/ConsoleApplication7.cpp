#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include <time.h>
using namespace std;
// note: EmperorSide = 100,000 FOR EVERY 1mm
// note: SlaveSide = 500,000 FOR EVERY 1mm

//basic functions:
void clearScreen()
{
	system("pause");
	system("cls");
}

void CardPick(char& PickCard, int& mm)
{
	cout << endl;
	cout << "Pick a card:";
	cin >> PickCard;
}

int EmperorCashOut(int& cash, int betWager)
{
	cout << "You win!" << endl;
	cash += betWager * 100000;    // 100,000
	cout << "You now have: " << cash << endl;
	clearScreen();
	return cash;  
}

int mmLoose(int betWager, int& mm)
{
	cout << "You lost..." << endl;
	mm -= betWager;
	cout << "You have lost: " << betWager << " mm" << endl;
	cout << endl;
	return mm;
}

int SlaveCashOut(int& cash, int betWager)
{
	cout << "You win!" << endl;
	cash += betWager * 500000;    // 500,000
	cout << "You now have: " << cash << endl;
	return cash;
}

void cardsLeftEmperor(vector<string> EmperorDeck, char& PickCard)
{
	for (int i = PickCard; i < EmperorDeck.size(); i++)
	{
		EmperorDeck.pop_back();
		cout << EmperorDeck[i] << endl;
	}
}
void cardsLeftSlave(vector<string> SlaveDeck, char& PickCard)
{
	for (int i = PickCard; i < SlaveDeck.size(); i++)
	{
		SlaveDeck.pop_back();
		cout << SlaveDeck[i] << endl;
	}
}
//-----------------------------------------------------------------
struct playerSide
{
	string yourSetOfDeck;
	string yourSide;
};

void EmperorSet(playerSide Emperor, vector<string>& EmperorDeck)
{
	Emperor.yourSide = "EMPEROR";
	cout << "Side: " << Emperor.yourSide << endl;

	// push back cards of the deck
	int numOfCivillianCard = 5;
	// Emperor card:
	EmperorDeck.push_back(" == Emperor");
	//Civillian card:
	for (int i = 0; i < numOfCivillianCard; i++)
	{
		EmperorDeck.push_back(" == Civillian");
		cout << "[" << i + 1 << "]" << EmperorDeck[i] << endl;
	}
}

void SlaveSet(playerSide Slave, vector<string>& SlaveDeck)
{
	Slave.yourSide = "Slave";
	cout << "Side: " << Slave.yourSide << endl;;
	//pushback cards
	int SlaveCivillian = 5;
	// Slave card:
	SlaveDeck.push_back(" == Slave");
	// Civillian card:
	for (int i = 0; i < SlaveCivillian; i++)
	{
		SlaveDeck.push_back(" == Civillian");
		cout << "[" << i + 1 << "]" << SlaveDeck[i] << endl;
	}
}

void EmperorRound(vector<string>& EmperorDeck, char& PickCard, int& cash, int betWager, int& mm)
{
	bool finish = false;
	do
	{
		//pick a card to play
		CardPick(PickCard, mm);
		int AIRandomCard = rand() % 100 + 1;
		cout << endl;
		//switch case:
		switch (PickCard)
		{
		case'1': cout << "-->> You picked a Emperor Card..." << endl;
			cout << endl;

			if (AIRandomCard <= 80)
			{
				cout << "AI picked a Civillian Card!" << endl;
				cout << endl;
				EmperorCashOut(cash, betWager);
				finish = true;
			}
			else
			{
				cout << "AI picked a Slave Card!" << endl;
				cout << endl;
				mmLoose(betWager, mm);
				finish = true;
			}
			break;

		case'2':
		case'3':
		case'4':
		case'5':cout << "-->> You picked a Civillian Card!" << endl;
			cout << endl;
			EmperorDeck.pop_back();
			if (AIRandomCard <= 80)
			{
				cout << "AI picked a Slave Card!" << endl;
				cout << endl;
				EmperorCashOut(cash, betWager);
				finish = true;
			}
			else
			{
				// draw
				cout << "AI picked a Civillian Card! " << endl;
				cout << endl;
				cout << "DRAW!" << endl;
				cout << endl;
				
				EmperorDeck.pop_back();
				for (int i = 0; i < EmperorDeck.size(); i++)
				{
					//EmperorDeck.pop_back();
					cout << "[" << i + 1 << "]" << EmperorDeck[i] << endl;

				}  
				
			}
			break;
			//EmperorDeck.pop_back();
			EmperorDeck.clear();
		}
	} while (!finish);
}

void SlaveRound(vector<string>& SlaveDeck, char& PickCard, int& cash, int betWager, int& mm)
{
	bool finish = false;
	do
	{
		//Pick a card to play
		CardPick(PickCard, mm);
		int AIRandomCard = rand() % 100 + 1;
		//switch case:
		switch (PickCard)
		{
		case'1':cout << "-->> You picked a Slave Card!" << endl;
			cout << endl;

			if (AIRandomCard <= 20)
			{
				cout << "AI picked a Emperor Card!" << endl;
				cout << endl;
				SlaveCashOut(cash, betWager);
				finish = true;
			}
			else
			{
				cout << "AI picked a Civillian" << endl;
				mmLoose(betWager, mm);
				finish = true;
			}
			break;

		case'2':
		case'3':
		case'4':
		case'5':cout << "-->> You picked Civillian Card!" << endl;
			cout << endl;
			SlaveDeck.pop_back();
			if (AIRandomCard <= 20)
			{
				cout << "AI picked a Emperor Card!" << endl;
				mmLoose(betWager, mm);
				finish = true;
			}
			else
			{
				cout << "AI picked a Civillian Card!" << endl;
				cout << "DRAW!" << endl;
				cout << endl;
				
				SlaveDeck.pop_back();
				for (int i = 0; i < SlaveDeck.size(); i++)
				{
					//SlaveDeck.pop_back();
					cout << "[" << i + 1 << "]" << SlaveDeck[i] << endl;
				}
			
			}
            break;
			//SlaveDeck.pop_back();
			SlaveDeck.clear();
		}
	} while (!finish);
}

int main()
{
	srand(time(NULL));
	vector<string> EmperorDeck;
	vector<string> SlaveDeck;
	playerSide Emperor, Slave;
	int totalRound = 12;
	int cash = 0;
	int mm = 30;
	int betWager;
	char pickCard; 
		//loop
	do 
	{
		// make for loop for the rounds untill 12 rounds
		for (int round = 1; round <= totalRound; round++)
		{
			if (mm == 0)
			{
				break;
			}
			//print cash and MM
			cout << "Cash: " << cash << endl;
			cout << "MM: " << mm << " mm" << endl;
			cout << "How many mm would you like to wager?: ";
			cin >> betWager;

			// clear screen
			clearScreen();

			//display round
			cout << "===================" << endl;
			cout << "Round: " << round << endl;
			cout << "===================" << endl;
			cout << endl;
			// put if-else loops OR Switch Cases:
			if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9)
			{
				// display EmperorDeck
				EmperorSet(Emperor, EmperorDeck);
				EmperorRound(EmperorDeck, pickCard, cash, betWager, mm);
				EmperorDeck.clear();
			}
			else //if (round == 4 || round == 5 || round == 6 || round == 10 || round == 11 || round == 12)
			{
				//display SlaveDeck
				SlaveSet(Slave, SlaveDeck);
				SlaveRound(SlaveDeck, pickCard, cash, betWager, mm);
				SlaveDeck.clear();
			}

		}
		
		// if (cash <= 20000000)
		if (cash <= 20000000 && mm != 0)
		{
			cout << "You Win!" << endl;
			break;  //
		}
		else if (cash >= 20000000 && mm != 0)
		{
			cout << "Meh!" << endl;
			break;
		}
		else
		{
			cout << "You lost everything..." << endl;
			break;
		}

	} while (mm != 0);

	return 0;
}


