#include "pch.h"
#include <iostream>
#include <string>
using namespace std;

void printArray(string items[8])
{
	for (int i = 0; i < 8; i++)
	{
		cout << items[i] << endl;
	}
}

int main()
{
	string items[8] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	printArray(items);

	string inputItem;
	int numberOfItem = 0;
	cout << endl;
	cout << "--------------" << endl;
	cout << "Enter an item: ";
	getline(cin, inputItem);

	for (int i = 0; i < 8; i++)
	{
		if (inputItem == items[i])
		{
			numberOfItem++;
		}
	}

	if (numberOfItem > 0)
	{
		cout << "There are " << numberOfItem << " " << inputItem << endl;
	}
	else
	{
		cout << "Item is not available." << endl;
	}



}
