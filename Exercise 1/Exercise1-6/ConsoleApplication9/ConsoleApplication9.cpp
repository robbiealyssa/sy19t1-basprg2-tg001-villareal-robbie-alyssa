#include "pch.h"
#include <iostream>
#include <string>
using namespace std;

void sortNumbers()
{
	int inputNums[10];
	int temp;

	for (int i = 0; i < 10; i++)
	{
		cin >> inputNums[i];
	}

	for (int i = 0; i < 10; i++)
	{
		for (int next = i; next < 10; next++)
		{
			if (inputNums[i] > inputNums[next])
			{
				temp = inputNums[i];
				inputNums[i] = inputNums[next];
				inputNums[next] = temp;
			}
		}
	}
	cout << endl;
	// after sorting output
	cout << "Sorted Numbers: " << endl;

	for (int i = 0; i < 10; i++)
	{
		cout << inputNums[i] << endl;
	}
}

int main()
{
	cout << "Enter 10 numbers to be sorted: " << endl;
	sortNumbers();
}

