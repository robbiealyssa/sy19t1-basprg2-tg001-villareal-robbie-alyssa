#include "pch.h"
#include <iostream>
#include <string>
using namespace std;

int getFactorial(int inputNumber)
{
	int factorial = 1;

	for (int i = 1; i <= inputNumber; i++)
		factorial = factorial * i;
		return factorial;
}

int main()
{
	int inputNumber, factorial = 1;
	cout << "Enter number: ";
	cin >> inputNumber;
	cout << "The factorial of " << inputNumber << " is " << getFactorial(inputNumber);
}