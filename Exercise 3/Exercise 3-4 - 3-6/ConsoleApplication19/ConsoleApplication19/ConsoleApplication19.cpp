#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
using namespace std;

struct lootItem
{
	string name;
	int gold;
};

lootItem* listOfItem(int randomItem, int& myTempGold)
{
	lootItem* item = new lootItem;
	randomItem = rand() % 5 + 1;

	if (randomItem == 1)
	{
		item->name = "Cursed Stone";
		item->gold = 0;
	}
	else if (randomItem == 2)
	{
		item->name = "Mithril Ore";
		item->gold = 500;
	}
	else if (randomItem == 3)
	{
		item->name = "Sharp Talon";
		item->gold = 50;
	}
	else if (randomItem == 4)
	{
		item->name = "Thick Leather";
		item->gold = 25;
	}
	else if (randomItem == 5)
	{
		item->name = "Jellopy";
		item->gold = 5;
	}

	cout << "You obtained: " << item->name << endl;
	cout << "Gold: " << item->gold << endl;
	return item;
}

// extra function
void clearScreen()
{
	system("pause");
	system("cls");
}
//
void enterDungeon(int myGold, int myTempGold, int randomItem)
{
	//start
	lootItem* item = new lootItem;
	char decision;
	myTempGold = 0;
	int multiplier = 1;

	while (myGold != 0 || myGold < 500)
	{
		clearScreen();
		item = listOfItem(randomItem, myTempGold);

		if (item->name != "Cursed Stone")
		{
			//cout << "temp gold: " << myTempGold << endl;
			myTempGold = (myTempGold + item->gold) * multiplier;
			cout << "You have: " << myTempGold << " Gold." << endl;
			multiplier++;
			clearScreen();
			cout << "Do you want to continue?(y/n): ";
			cin >> decision;
			if (decision == 'n' || decision == 'N')
			{
				myGold += myTempGold;
				cout << "You now have a total of: " << myGold << " Gold." << endl;  //ask if want to enter again
				break;
			}

		}
		else
		{
			myTempGold *= 0;
			cout << "You encountered a Cursed Stone..." << endl;
			cout << "Thank you for playing." << endl;
			cout << "Want to enter again?";
			cin >> decision;
			if (decision == 'y' || decision == 'Y')
			{
				clearScreen();

				myGold -= 25;
				cout << "You now only have: " << myGold << " Gold" << endl; // cannot reset temporary gold
			}
			else if (decision == 'n' || decision == 'N')
			{
				cout << "Thank you for playing." << endl;
				exit(1);
				break;
			}
		}

		switch (decision)
		{
		case'y':
		case'Y':
			//
			break;
		}
	}

}

int main()
{
	srand(time(NULL));

	int my_Gold = 50;
	int myTemp_Gold = 0;
	int randomItem = NULL;

	cout << "Welcome!" << endl;
	clearScreen();
	cout << "Gold: " << my_Gold << " Gold" << endl;
	cout << "25 Gold is needed to be able to enter the dungeon." << endl;
	clearScreen();

	while (my_Gold != 0)
	{
		my_Gold -= 25;

		cout << "You now only have: " << my_Gold << " Gold" << endl;
		clearScreen();

		cout << "Welcome to the dungeon." << endl;
		clearScreen();

		enterDungeon(my_Gold, myTemp_Gold, randomItem);

		if (my_Gold >= 500)
		{
			cout << "You won!" << endl;
		}
		else if (my_Gold <= 0)
		{
			cout << "Game Over!" << endl;
		}

	}

	return 0;
}