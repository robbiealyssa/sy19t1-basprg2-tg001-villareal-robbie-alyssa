#include "pch.h"
#include <iostream>
#include <string>
#include "time.h"
#include "Character.h"

using namespace std;

void clearScreen(){
	system("pause");
	system("cls");
}
void printVs() {
	cout << endl;
	cout << "VS" << endl;
	cout << endl;
}

int main(){

	srand(time(NULL));
	string playerName;
	int hp = rand() % 20 + 5;
	int pow = rand() % 10 + 1;
	int	vit = rand() % 5 + 1;
	int	agi = rand() % 10 + 1;
	int	dex = rand() % 5 + 1;
	char pickClass;

	srand(time(NULL));
	string EnemyClass[3] = { "Warrior","Assassin","Mage"};
	int rndEnemyClass = rand() % 3;
	string classEnemy = EnemyClass[rndEnemyClass];
	string enemyName = "Enemy";

	cout << "Enter you name: ";
	cin >> playerName;

	Character* Warrior = new Character(playerName, "Warrior", 20, 10, 4, 7, 3);
	Character* Assassin = new Character(playerName, "Assassin", 20, 8, 5, 9, 5);
	Character* Mage = new Character(playerName, "Mage", 20, 7, 5, 6, 5);
	Character* Enemy = new Character(enemyName, EnemyClass[rndEnemyClass], hp, pow, vit, agi, dex);

	cout << "===================" << endl;
	cout << "[1] == Warrior" << endl;
	cout << "[2] == Assassin" << endl;
	cout << "[3] == Mage" << endl;
	cout << "===================" << endl;

	cout << "Pick a Class: " << endl;
	cin >> pickClass;

	srand(time(NULL));
	int rndNumAttck1 = rand() % 10 + 1;
	int rndNumAttck2 = rand() % 10 + 1;

	switch (pickClass) {
	case'1': Warrior->displayStats(playerName);
		printVs();
		Enemy->displayEnemyStats(enemyName);
		cout << "Initiating Combat..." << endl;
		clearScreen();

		if (Warrior->getAgi() >= Enemy->getAgi()) {
			while (Warrior->getHp() > 0 && Enemy->getHp() > 0) {
				cout << Warrior->getPlayerName() << " dealt " << rndNumAttck1 << " damage to " << Enemy->getPlayerName() << "." << endl;
				Enemy->getDamage(rndNumAttck1);
				system("pause");
				cout << Enemy->getPlayerName() << " dealt " << rndNumAttck2 << " damage to " << Warrior->getPlayerName() << "." << endl;
				Warrior->getDamage(rndNumAttck2);
				system("pause");
			}
		}
		else if (Warrior->getAgi() < Enemy->getAgi()) {
			while (Warrior->getHp() > 0 && Enemy->getHp() > 0) {
				cout << Enemy->getPlayerName() << " dealt " << rndNumAttck2 << " damage to " << Warrior->getPlayerName() << "." << endl;
				Warrior->getDamage(rndNumAttck2);
				system("pause");
				cout << Warrior->getPlayerName() << " dealt " << rndNumAttck1 << " damage to " << Enemy->getPlayerName() << "." << endl;
				Enemy->getDamage(rndNumAttck1);
				system("pause");
			}
		}
		if (Enemy->getPlayerClass() == "Assassin" && Warrior->getHp() >= 0) {
			Warrior->warriorWin();
		}
		clearScreen();
		Warrior->displayStats(playerName);
		break;

	case'2': Assassin->displayStats(playerName);
		printVs();
		Enemy->displayEnemyStats(enemyName);
		cout << "Initiating Combat..." << endl;

		clearScreen();

		if (Assassin->getAgi() >= Enemy->getAgi()) {
			while (Assassin->getHp() > 0 && Enemy->getHp() > 0) {
				cout << Assassin->getPlayerName() << " dealt " << rndNumAttck2 << " damage to " << Enemy->getPlayerName() << "." << endl;
				Enemy->getDamage(rndNumAttck2);
				system("pause");
				cout << Enemy->getPlayerName() << " dealt " << rndNumAttck1 << " damage to " << Assassin->getPlayerName() << "." << endl;
				Assassin->getDamage(rndNumAttck1);
				system("pause");
			}
		}
		else if (Assassin->getAgi() < Enemy->getAgi()) {
			while (Assassin->getHp() > 0 && Enemy->getHp() > 0) {
				cout << Enemy->getPlayerName() << " dealt " << rndNumAttck1 << " damage to " << Assassin->getPlayerName() << "." << endl;
				Assassin->getDamage(rndNumAttck1);
				system("pause");
				cout << Assassin->getPlayerName() << " dealt " << rndNumAttck2 << " damage to " << Enemy->getPlayerName() << "." << endl;
				Enemy->getDamage(rndNumAttck2);
				system("pause");
			}
		}
		if (Enemy->getPlayerClass() == "Mage" && Assassin->getHp() >= 0) {
			Assassin->assassinWin();
		}
		clearScreen();
		Assassin->displayStats(playerName);
		break;

	case'3':Mage->displayStats(playerName);
		printVs();
		Enemy->displayEnemyStats(enemyName);
		cout << "Initiating Combat..." << endl;

		clearScreen();

		if (Mage->getAgi() >= Enemy->getAgi()) {
			while (Mage->getHp() > 0 && Enemy->getHp() > 0) {
				cout << Mage->getPlayerName() << " dealt " << rndNumAttck2 << " damage to " << Enemy->getPlayerName() << "." << endl;
				Enemy->getDamage(rndNumAttck2);
				system("pause");
				cout << Enemy->getPlayerName() << " dealt " << rndNumAttck1 << " damage to " << Mage->getPlayerName() << "." << endl;
				Mage->getDamage(rndNumAttck1);
				system("pause");
			}
		}
		else if (Mage->getAgi() < Enemy->getAgi()) {
			while (Mage->getHp() > 0 && Enemy->getHp() > 0) {
				cout << Enemy->getPlayerName() << " dealt " << rndNumAttck1 << " damage to " << Mage->getPlayerName() << "." << endl;
				Mage->getDamage(rndNumAttck1);
				system("pause");
				cout << Mage->getPlayerName() << " dealt " << rndNumAttck2 << " damage to " << Enemy->getPlayerName() << "." << endl;
				Enemy->getDamage(rndNumAttck2);
				system("pause");
			}
		}
		if (Enemy->getPlayerClass() == "Warrior" && Mage->getHp() >= 0) {
			Mage->mageWin();
		}
		clearScreen();
		Mage->displayStats(playerName);
		break;
	}

		clearScreen();

	delete Warrior;
	delete Assassin;
	delete Mage;
	delete Enemy;

	return 0;
}