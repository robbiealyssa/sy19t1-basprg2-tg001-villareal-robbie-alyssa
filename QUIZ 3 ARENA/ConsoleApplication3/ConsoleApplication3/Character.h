#pragma once
#include <iostream>
#include <string>

using namespace std;

class Character{
public:
	
	Character();
	Character(string name, string playerClass, int hp, int pow, int vit, int agi, int dex);

	// try array for stats
	void displayStats(string name);
	void displayEnemyStats(string name);

	string getPlayerName();
	string getPlayerClass();


	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	
	int getHitRate();
	//int getDamage(int rndDamage);
	void getDamage(int rndDamage);
	void getDamageWithBonus(int rndDamage);

	//bonus if win
	void warriorWin();
	void assassinWin();
	void mageWin();

private:
	string mName;
	string mPlayerClass;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;

};

