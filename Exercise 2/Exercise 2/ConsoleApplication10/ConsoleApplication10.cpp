#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
using namespace std;

//extra functions::::
void clearScreen()
{
	system("pause");
	system("cls");
}
void phraseAIWin()
{
	cout << "AI Wins!" << endl;
}
void phrasePlayerWin()
{
	cout << "You win!" << endl;
}
void lines()
{
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
}

// 2-1 "Bet"
int userBetValue(int& userInputBet, int& userGold)
{
	userGold -= userInputBet;
	cout << "You now have: " << userGold << endl;
	return userGold;
}

void userInputBet(int& userInputBet, int& userGold)
{
	userGold -= userInputBet;
}

//2-2 "Dice Roll"
void DiceRoll(int& diceOne, int& diceTwo)
{
	diceOne = rand() % 6 + 1;
	diceTwo = rand() % 6 + 1;
	cout << diceOne << " , " << diceTwo << endl;
}

//2-3 "Evaluate to pay out" 
int AITotalDiceRoll(int& diceOne, int& diceTwo, int& totalOfDice)
{
	totalOfDice = diceOne + diceTwo;
	cout << "AI's Total Dice => " << totalOfDice << endl;
	return totalOfDice;
}
int PlayerTotalDiceRoll(int& diceOne, int& diceTwo, int& totalOfDice)
{
	totalOfDice = diceOne + diceTwo;
	cout << "Your Total Dice => " << totalOfDice << endl;
	return totalOfDice;
}

int cashOut(int& userInputBet, int& userGold)
{
	userGold = (userGold + userInputBet);
	cout << "You now have: " << userGold << endl;
	return userGold;
}

int snakeEyesBonus(int& userInputBet, int& userGold)
{
	userGold = (userGold + userInputBet) * 3;
	cout << "Snake EyEs!! " << endl
		<< "You now have: " << userGold << endl;
	return userGold;
}

//------------------------------------------------------------------
// 2-4 "PlayRound"
void playeRound()
{
	srand(time(NULL));
	int user_Gold = 1000;
	int user_Bet = NULL;
	int user_DiceOne, user_DiceTwo;
	int AI_DiceOne, AI_DiceTwo;
	int AI_TotalDice = NULL;
	int user_TotalDice = NULL;

	while (user_Gold != 0)
	{
		cout << "Your Gold: " << user_Gold << endl;
		cout << "How much wolud you like to bet?: ";
		cin >> user_Bet;
		userBetValue(user_Bet, user_Gold);

		clearScreen();

		// start of rolling dice
		lines();//--------------------------------------------------
		cout << "AI Rolls the Dice ::: ";
		DiceRoll(AI_DiceOne, AI_DiceTwo);
		AITotalDiceRoll(AI_DiceOne, AI_DiceTwo, AI_TotalDice);
		lines();//--------------------------------------------------
		cout << "Player Rolls the Dice ::: ";
		DiceRoll(user_DiceOne, user_DiceTwo);
		PlayerTotalDiceRoll(user_DiceOne, user_DiceTwo, user_TotalDice);
		lines();//--------------------------------------------------

		//determine who has higher total
		if (AI_TotalDice > user_TotalDice)
		{
			phraseAIWin();
		}
		else if (AI_TotalDice < user_TotalDice)
		{
			phrasePlayerWin();
			cashOut(user_Bet, user_Gold);
		}
		cout << endl;

		// if both sides same total
		if (AI_TotalDice == user_TotalDice)
		{
			cout << "DRAW!" << endl;
		}

		if (AI_TotalDice == 2)           // or (AI_DiceOne == 1 && AI_DiceTwo == 1)
		{
			phraseAIWin();

		}
		else if (user_TotalDice == 2)    // or (user_DiceOne == 1 && user_DiceTwo == 1)
		{
			phrasePlayerWin();
			snakeEyesBonus(user_Bet, user_Gold);
		}

	}

}

int main()
{
	playeRound();
	return 0;
}


