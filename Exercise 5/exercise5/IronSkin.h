#pragma once
#include "Buff.h"
#include <iostream>
#include <string>

using namespace std;

class IronSkin : public Buff
{
public:
	IronSkin();
	~IronSkin();

	void setBuff(Player* player);
private:
	int vitAdd;
};

