#include "pch.h"
#include "Heal.h"
#include"Player.h"
#include<iostream>
#include<string>

using namespace std;

Heal::Heal(){
	hpAdd = 10;
}

Heal::~Heal(){
}

void Heal::setBuff(Player * player){
	cout << "Obtained Heal" << endl;
	player->addHp(this->hpAdd);
}
