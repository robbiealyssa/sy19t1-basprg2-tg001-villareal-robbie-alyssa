#pragma once
#include "Buff.h"
#include <iostream>
#include <string>

using namespace std;

class Concentration : public Buff
{
public:
	Concentration();
	~Concentration();

	void setBuff(Player* player);

private: 
	int conAdd;
};

