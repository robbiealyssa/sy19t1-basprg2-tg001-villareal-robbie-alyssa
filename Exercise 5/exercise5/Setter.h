#pragma once
#include "Buff.h"
#include "Player.h"
#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Buff;
class Player;
class Setter
{
public:
	Setter();
	~Setter();

	void obtainedBuff(Player* player);

private:
	vector<Buff*>addBuff;
};

