#include "pch.h"
#include "Player.h"
#include "Buff.h"

//Player::Player(){
//	mHp = getHp();
//	mPow = getPow();
//	mVit = getVit();
//	mDex = getDex();
//	mAgi = getAgi();
//}

Player::Player(int hp, int pow, int vit, int dex, int agi){
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mDex = dex;
	mAgi = agi;
}

Player::~Player(){
}

int Player::getHp(){
	return mHp;
}
int Player::getPow(){
	return mPow;
}
int Player::getVit(){
	return mVit;
}
int Player::getDex(){
	return mDex;
}
int Player::getAgi(){
	return mAgi;
}

int Player::addHp(int aHp){

	mHp += aHp;
	return mHp;
}

int Player::addPow(int aPow){
	mPow += aPow;
	return mPow;
}

int Player::addVit(int aVit){
	mVit += aVit;
	return mVit;
}

int Player::addDex(int aDex){
	mDex += aDex;
	return mDex;
}

int Player::addAgi(int aAgi){
	mAgi += aAgi;
	return mAgi;
}

void Player::displayStats() {
	cout << "HP: " << getHp() << endl;
	cout << "POW: " << getPow() << endl;
	cout << "VIT: " << getVit() << endl;
	cout << "DEX: " << getDex() << endl;
	cout << "AGI: " << getAgi() << endl;
}