#include "pch.h"
#include "Haste.h"
#include "Buff.h"
#include "Player.h"


Haste::Haste(){
	agiAdd = 2;
}

Haste::~Haste()
{
}

void Haste::setBuff(Player * player){
	cout << "Obtained Haste" << endl;
	player->addAgi(this->agiAdd);
}
