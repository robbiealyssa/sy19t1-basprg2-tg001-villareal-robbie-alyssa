#include "pch.h"
#include "Setter.h"
#include "Player.h"
#include "Buff.h"
#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Concentration.h"
#include "Haste.h"
#include <time.h>

Setter::Setter(){
	addBuff.push_back(new Heal);
	addBuff.push_back(new Might);
	addBuff.push_back(new IronSkin);
	addBuff.push_back(new Concentration);
	addBuff.push_back(new Haste);
}

Setter::~Setter(){

}

void Setter::obtainedBuff(Player * player){
	//srand(time(NULL));
	int rndBuff = rand() % 5;
	addBuff[rndBuff]->setBuff(player);

}

