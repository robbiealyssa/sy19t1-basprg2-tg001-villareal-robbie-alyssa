#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
#include "Player.h"
#include "Setter.h"
#include "Buff.h"
#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Concentration.h"
#include "Haste.h"
#include <time.h>

using namespace std;

void cls() {
	system("pause");
	system("cls");
}

int main()
{
	srand(time(NULL));
	Player* player = new Player(100, 11, 10, 17, 9);
	Setter* setToPlayer = new Setter();

	while (true) {
		player->displayStats();

		system("pause");

		cout << "..." << endl;

		setToPlayer->obtainedBuff(player);
		cls();

	}

	return 0;
}

