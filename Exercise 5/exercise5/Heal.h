#pragma once
#include "Buff.h"
#include <iostream>
#include <string>

using namespace std;

class Heal:public Buff
{
public:
	Heal();
	~Heal();

	void setBuff(Player* player);

private:
	int hpAdd;
};

