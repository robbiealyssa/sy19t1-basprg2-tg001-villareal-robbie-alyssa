#pragma once
#include "Buff.h"
#include <iostream>
#include <string>

using namespace std;

class Might : public Buff
{
public:
	Might();
	~Might();

	void setBuff(Player* player);
private:
	int powAdd;
};

