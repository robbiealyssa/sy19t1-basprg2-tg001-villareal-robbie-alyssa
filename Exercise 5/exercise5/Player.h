#pragma once
#include "Buff.h"
#include "Setter.h"
#include <iostream>
#include <string>

using namespace std;

class Buff;
class Player
{
public:
	/*Player();*/
	Player(int hp, int pow, int vit, int dex, int agi);
	~Player();

	int getHp();
	int getPow();
	int getVit();
	int getDex();
	int getAgi();

	int addHp(int aHp);
	int addPow(int aPow);
	int addVit(int aVit);
	int addDex(int aDex);
	int addAgi(int aAgi);

	void displayStats();

private:
	int mHp;
	int mPow;
	int mVit;
	int mDex;
	int mAgi;
};

