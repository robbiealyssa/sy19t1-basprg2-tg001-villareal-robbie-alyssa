#pragma once
#include "Shape.h"
#include <string>

using namespace std;

class Circle : public Shape
{
public:
	Circle();
	~Circle();

	int getLengthOfCircle();
	void setLengthOfCircle(int value);

	int getArea() override;


private:
	int mLengthOfCircle;
	
};

