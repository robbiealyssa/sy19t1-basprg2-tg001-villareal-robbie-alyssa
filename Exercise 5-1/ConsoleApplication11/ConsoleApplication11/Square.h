#pragma once
#include "Shape.h"
#include <string>

using namespace std;

class Square : public Shape
{
public:
	Square();
	~Square();

	int getLengthOfSquare();
	void setLengthOfSqaure(int value);

	int getArea() override;

private: 
	int mLengthOfSquare;

};

