#include "pch.h"
#include <iostream>
#include "Shape.h"
#include "Square.h"
#include "Rectangle.h"
#include "Circle.h"
#include <vector>

using namespace std;

void displayShape(Shape* shape) {
	cout << "Name of Shape: " << shape->getShapeName() << endl;
	cout << "Number Of Sides: " << shape->getNumOfSides() << endl;
	cout << "Area of the Shape: " << shape->getArea() << endl;
}

int main(){
	vector<Shape*> shapes;

	Square* square = new Square();
	square->setShapeName("Square");
	square->setNumOfSides(4);
	square->setLengthOfSqaure(5);

	Rectangle* rectangle = new Rectangle();
	rectangle->setShapeName("Rectangle");
	rectangle->setNumOfSides(4);
	rectangle->setLengthOfRectangle(4);

	Circle* circle = new Circle();
	circle->setShapeName("Circle");
	circle->setNumOfSides(1);
	circle->setLengthOfCircle(1);
	

	//displayShape(square);

	shapes.push_back(square);
	shapes.push_back(circle);
	shapes.push_back(rectangle);

	for (int i = 0; i < shapes.size(); i++) {
		Shape* shape = shapes[i];
		cout << shape->getShapeName() << endl;
		cout << "Sides: " << shape->getNumOfSides() << endl;
		cout << "Area: " << shape->getArea() << endl;

	}

	return 0;

}
