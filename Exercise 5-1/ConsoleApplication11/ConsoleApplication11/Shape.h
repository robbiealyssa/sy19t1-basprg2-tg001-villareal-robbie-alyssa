#pragma once
#include <string>

using namespace std;

class Shape
{
public:
	Shape();
	~Shape();

	string getShapeName();
	int getNumOfSides();

	void setShapeName(string shapeName);
	void setNumOfSides(int numOdSides);

	virtual int getArea();

private: 
	string mShapeName;
	int mNumOfSides;
};

