#pragma once
#include "Shape.h"
#include <string>

using namespace std;

class Rectangle : public Shape
{
public:
	Rectangle();
	~Rectangle();

	int getLengthOfRectangle();
	int getWidthOfRectangle();

	void setLengthOfRectangle(int length);
	void setWidthOfRectangle(int width);

	int getArea() override;

private: 
	int mLengthOfRectangle;
	int mWidthOfRectangle;
};

