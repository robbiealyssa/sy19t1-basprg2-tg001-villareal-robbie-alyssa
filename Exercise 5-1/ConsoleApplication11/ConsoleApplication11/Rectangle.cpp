#include "pch.h"
#include "Rectangle.h"


Rectangle::Rectangle()
{
}


Rectangle::~Rectangle()
{
}

int Rectangle::getLengthOfRectangle()
{
	return mLengthOfRectangle;
}

int Rectangle::getWidthOfRectangle()
{
	return mWidthOfRectangle;
}

void Rectangle::setLengthOfRectangle(int length)
{
	mLengthOfRectangle = length;
}

void Rectangle::setWidthOfRectangle(int width)
{
	mWidthOfRectangle = width;
}

int Rectangle::getArea()
{
	return mLengthOfRectangle* mWidthOfRectangle;
}
