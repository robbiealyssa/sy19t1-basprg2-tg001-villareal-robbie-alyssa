#pragma once
#include <string>

using namespace std;

class Spell;
class Wizard
{
public:
	string WizardName;
	int WizardHp;
	int WizardMp;

	Spell* currentSpell;

	void castSpell(Wizard * targetOpponent);
};

