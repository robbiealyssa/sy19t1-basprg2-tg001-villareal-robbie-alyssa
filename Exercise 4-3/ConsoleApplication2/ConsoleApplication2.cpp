#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
#include "Spell.h"
#include "Wizard.h"

using namespace std;

void clearScreen() {
	system("pause");
	system("cls");
}

int main()
{
	srand(time(NULL));

	string inputName;

	Wizard* WizardOne = new Wizard;
	Wizard* WizardTwo = new Wizard;
	Spell* FireBall = new Spell;

	//HP:
	WizardOne->WizardHp = 100;
	WizardTwo->WizardHp = 100;
	
	//MP:
	WizardOne->WizardMp = 100;
	WizardTwo->WizardMp = 100;
	
	//FireBall:
	FireBall->SpellDmg = 20;
	FireBall->SpellMpCost = 10;

	WizardOne->currentSpell = FireBall;
	WizardTwo->currentSpell = FireBall;

	//WizardOne:
	cout << "Enter Wizard One name: ";
	cin >> WizardOne->WizardName;
	cout << endl;
	cout << "HP: " << WizardOne->WizardHp;
	cout << endl;
	cout << "MP: " << WizardOne->WizardMp;
	cout << endl;

	clearScreen();

	//WizardTwo:
	cout << "Enter Wizard Two name: ";
	cin >> WizardTwo->WizardName;
	cout << endl;
	cout << "HP: " << WizardTwo->WizardHp;
	cout << endl;
	cout << "MP: " << WizardTwo->WizardMp;

	cout << endl;
	clearScreen();

	//loop
	while (WizardOne->WizardHp > 0 || WizardTwo->WizardHp > 0 )
	{
		// random player
		int randWizard = rand() % 2 + 1;
		cout << endl;
		// selected plater attacks first 
		if (randWizard == 1)
		{
			cout << WizardOne->WizardName << " is gonna to attck" << endl;
			WizardOne->castSpell(WizardTwo);
			// after attck, show stats
			cout << endl;
			cout << "STATS: " << endl;
			cout << endl;
			cout << WizardTwo->WizardName << ":" << endl;
			cout << "HP: " << WizardTwo->WizardHp << endl;
			cout << "MP: " << WizardTwo->WizardMp << endl;

			cout << WizardOne->WizardName << ":" << endl;
			cout << "HP: " << WizardOne->WizardHp << endl;
			cout << "MP: " << WizardOne->WizardMp << endl;

			system("pause");
		}

		else if (randWizard == 2)
		{
			cout << WizardTwo->WizardName << " is gonna to attck" << endl;
			WizardTwo->castSpell(WizardOne);
			// after attck, show stats
			cout << endl;
			cout << "STATS: " << endl;
			cout << endl;
			cout << WizardOne->WizardName << ":" << endl;
			cout << "HP: " << WizardOne->WizardHp << endl;
			cout << "MP: " << WizardOne->WizardMp << endl;

			cout << WizardTwo->WizardName << ":" << endl;
			cout << "HP: " << WizardTwo->WizardHp << endl;
			cout << "MP: " << WizardTwo->WizardMp << endl;

			system("pause");
		}
		
	}

	//delete 
	delete WizardOne;
	delete WizardTwo;

	return 0;
}
